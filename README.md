# Soal Shift Sisop Modul 4 F01 2022



## Anggota Kelompok
1. Angela Oryza Prabowo
2. Azzura Ferliani Ramadhani
3. Naufal Adli Purnama

## Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:

   + `a.` Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat **huruf besar** akan ter encode dengan **atbash cipher** dan huruf kecil akan terencode dengan **rot13** <br>
   **Contoh : <br>
    “Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”**
   + `b.` Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
   + `c.` Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.
   + `d.` Setiap data yang terencode akan masuk dalam file **“Wibu.log”** <br> 
         Contoh isi: <br>
         RENAME terenkripsi **/home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat** <br>
RENAME terdecode **/home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba**
   + `e.` Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif)

### Penyelesaian
  + `a dan b` <br>
  >Program akan melakukan pengecekan pada setiap *directory* yang ditemukan pada operasi `readdir()` yang terdapat pada program. Apabila nama *directory* tersebut mengandung kata "Animeku_", maka program akan melakukan enkripsi pada file yang terdapat dalam *directory* tersebut. <br>
  ```c
  char awalan[3][16] = {"Animeku_", "IAN_", "nds_"};
  if ((strCheck = strstr(path, awalan[0])) != NULL) {
		flag = 1;
		if (strCheck != NULL)
			wibu_decode(strCheck);
	}
    //kode laim
	while ((dir = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if (strCheck != NULL) {
			if (flag == 1) wibu_encode(dir->d_name);
    //kode lain
	}
```
  > Apabila di dalam nama file tersebut terdapat huruf *upper case*, maka huruf tersebut akan diencode dengan menggunakan **atbash cipher**. **atbash cipher** sendiri berarti mengubah sebuah karakter menjadi kebalikannya (ex. 'A' = 'Z'). Oleh karena itu, pendekatan yang digunakan adalah dengan menjumlahkan ASCII huruf kapital terbesar dengan terkecil kemudian menguranginya dengan ASCII huruf tersebut.
  ```c
  if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] = 'Z' + 'A' - str[i];
```
> Sedangkan, apabila dalam nama file tersebut terdiri dari huruf *lower case*, maka huruf tersebut akan dienkripsi dengan menggunakan **rot13** dimana huruf tersebut akan diubah menjadi huruf dengan urutan ke-13 setelahnya. Pendekatan yang digunakan adalah menjadikan ASCII karakter tersebut menjadi urutan pertama dengan menguranginya dengan ASCII huruf 'a', kemudian menambahkannya dengan 13, daan dimodulo dengan 26 sehingga memungkinkan untuk terjadinya perulangan.
```c
if (str[i] >= 'a' && str[i] <= 'z')
			str[i] = 'a' + (str[i] -'a' + 13) % 26;
```
   + `c.`
   > Melakukan pengecekan dua kali pada sebuah *directory*. Jika pada pengecekan pertama dan kedua nama *directory* tersebut masih mengandung kata "Animeku_", maka nama file di dalam *directory tersebut akan ikut terdekripsi.
   ```c
 	char *strCheck;
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		if (strCheck != NULL) wibu_decode(strCheck);  
```
   + `d dan e` 
   > Jika ketika melakukan fungsi `rename()`, nama *directory* sumber mengandung kata "Animeku_", maka operasi yang dilakukan adalah dekripsi. Sedangkan, apabila nama *directory* tujuan yang mengandung kata "Animeku_", maka operasi yang dilakukan adalah enkripsi. 
   ```c
 	void wibuLog(const char *lama, char *baru, int tipe) {
	char *namaLog = "Wibu.log";
	char logPath[100];
	sprintf(logPath, "%s/%s", dirPath, namaLog);
	FILE *logFile = fopen(logPath, "a");

	if (tipe == 0)
		fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", lama, baru);
	else if (tipe == 1)
		fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", lama, baru);
	else if (tipe == 2)
		fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", lama, baru);
	fclose(logFile);
    }

        static int xmp_rename(const char *sumber, const char *tujuan) {
	int flag;
	if (strstr(sumber, awalan[0]) != NULL) flag = 1;
    // kode lain
	if (flag == 1) {
		if (strstr(fileTujuan, awalan[0]) != NULL)
			wibuLog(fileSumber, fileTujuan, 1);
		else if (strstr(fileSumber, awalan[0]) != NULL) 
			wibuLog(fileSumber, fileTujuan, 2);
		result = rename (fileSumber, fileTujuan);
	}
    //kode lain
```

### Dokumentasi

+ `a.` dan `b.` hasil enkripsi setelah penamaan ulang
  1. isi animeku
    ![isi animeku](/doku/soal1/AB1.png) 
  2. isi Animeku_
   ![isi Animeku_](/doku/soal1/AB2.png) 

+ `c.` hasil wibu_decode()
  1. isi Animeku_
    ![isi Animeku_](/doku/soal1/C1.png) 
  2. isi animeku
   ![isi animeku](/doku/soal1/C2.png) 

+ `d.` dan `e.`
  1. sebelum `mkdir Animeku_`
    ![kondisi awal dir](/doku/soal1/DE1.png)
    ![kondisi awal log](/doku/soal1/DE2.png)

  2. setelah `mv anime Animeku_`
   ![setelah `mv anime Animeku_` dir](/doku/soal1/DE3.png) 
   ![setelah `mv anime Animeku_` log](/doku/soal1/DE4.png) 

  3. setelah `mv Animeku_ anime`
   ![setelah `mv anime Animeku_` dir](/doku/soal1/DE5.png) 
   ![setelah `mv anime Animeku_` log](/doku/soal1/DE6.png) 

## Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya
untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius
sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya
untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :

   + `a.` Jika suatu direktori dibuat dengan awalan **“IAN_[nama]”**, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma **Vigenere Cipher** dengan key “INNUGANTENG” (Case-sensitive, Vigenere).
   + `b.` Jika suatu direktori di rename dengan **“IAN_[nama]”**, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.
   + `c.` Apabila nama direktori dihilangkan **“IAN_”**, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.
   + `d.` Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori **“/home/[user]/hayolongapain_[kelompok].log”**. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.
   + `e.` Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level **INFO** dan **WARNING**. Untuk log level **WARNING**, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut :
       
        [Level]::[dd][mm][yyyy]-[HH]:[MM]:[SS]::[CMD]::[DESC :: DESC]

### Penyelesaian

+ `a.` secara singkat, vigenere cipher mengenkripsi sebuah string dengan menggunakan string kunci. Urutan abjad karakter string[i] inputan akan ditambahkan urutan abjad karakter string_kunci[i]. Hasil tersebut selanjutnya disesuaikan apakah haruf besar atau kecil. Apabila karakter string[i] dimana i lebih besar dari panjang string_kunci, maka akan kembali ke karakter pertama string_kunci.
    ```c
    void vigenere_encode (char *str) {
        if (strcmp(str, ".") == 0 || strcmp(str,"..") == 0) return;
        int length = strlen(str);
        char key[] = "INNUGANTENG";
        int key_len = strlen(key);

        int i;
        for(i = 0; i < length; i++) {
            if (str[i] == '/') continue;
            if((str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)){
                int is_capital = 0;
                if(str[i] >= 65 && str[i] <= 90) is_capital = 1;
                if(is_capital){
                    str[i] = ((str[i] + key[i % (key_len)] - 130) % 26 + 65);
                }else{
                    str[i] = ((str[i] + key[i % (key_len)] + 32 - 194) % 26 + 97);
                }
            }
        }
    }
    ```

+ `b.` diselesaikan dengan memodifikasi fungsi `readdir()`, ketika menemukan path yang mengandung 'IAN_' maka akan menyalakan flag dengan kode 2. Direktori dengan flag 2 akan menjalankan fungsi `vigenere_encode()` pada setiap path dalam dir tersebut.
+ `c.` diselesaikan dengan membuat fungsi `vigenere_decode()` yang membalik algoritma `vigenere_encode()` yang terlihat seperti berikut:
  ```c
  void vigenere_decode (char *str) {
        if (strcmp(str,".") == 0 || strcmp(str,"..") == 0 || strstr(str, "/") == NULL)
            return;

        char key[] = "INNUGANTENG";
        int key_len = strlen(key);

        int length = strlen(str), 
            s = 0;
        for (int i= length; i>=0; i--) {
            if (str[i] == '/') break;
            if (str[i] == '.') {
                length = i;
                break;
            }
        }for (int i= 0; i < length; i++) {
            if (str[i] == '/') {
                s = i+1;
                break;
            }
        }for(int i = s; i < length; i++) {
            if((str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)){
                int is_capital = 0;
                if(str[i] >= 65 && str[i] <= 90) is_capital = 1;
                if(is_capital){
                    str[i] = str[i] = (str[i] - (key[i % (key_len)]) < 0) ? str[i] - (key[i % (key_len)]) + 26 + 65 : str[i] - (key[i % (key_len)]) + 65;
                }else{
                    str[i] = (str[i] - (key[i % (key_len)] + 32) < 0) ? str[i] - (key[i % (key_len)] + 32) + 26 + 97 : str[i] - (key[i % (key_len)] + 32) + 97;
                }
            }
        }
    }
  ```
+ `d.` diselesaikan dengan membuat fungsi `IANLog()` yang membuka file pada direktori yang diinginkan dengan kode seperti berikut:
    ```c
    char *namaLog = "hayolongapain_F01.log";
    char logPath[100];
    sprintf(logPath, "%s/%s", dirPath, namaLog);
    FILE *logFile = fopen(logPath, "a");
    ```
+ `e.` diselesaikan dengan membuat fungsi `IANLog()` yang menerima argumen path lama, path baru, dan tipe command. Library `<time.h>` digunakan untuk mendapatkan waktu pemanggilan fungsi yang akan dimasukkan sebagai salah satu metric entri log. 
    ```c
    void IANLog(const char *lama, const char *baru, int tipe) {
        char *namaLog = "hayolongapain_F01.log";
        char logPath[100];
        sprintf(logPath, "%s/%s", dirPath, namaLog);
        FILE *logFile = fopen(logPath, "a");

        struct tm* ptr;
        time_t tm = time(NULL);
        ptr = localtime(&tm);
        int arr[6];
        arr[0] = ptr->tm_mday;
        arr[1] = ptr->tm_mon;
        arr[2] = ptr->tm_year;
        arr[3] = ptr->tm_hour;
        arr[4] = ptr->tm_min;
        arr[5] = ptr->tm_sec;

        char timestr[16];
        timestr[0] = (arr[0] > 9) ? arr[0] / 10 + '0' : '0'; 
        timestr[1] = arr[0] % 10 + '0';
        timestr[2] = (arr[1] > 9) ? arr[0] / 10 + '0' : '0';
        timestr[3] = arr[1] % 10 + '0';
        timestr[4] = (arr[2] > 9) ? arr[0] / 10 + '0' : '0';
        timestr[5] = arr[2] % 10 + '0';
        timestr[6] = '-';
        timestr[7] = (arr[3] > 9) ? arr[0] / 10 + '0' : '0';
        timestr[8] = arr[3] % 10 + '0';
        timestr[9] = ':';
        timestr[10] = (arr[4] > 9) ? arr[0] / 10 + '0' : '0';
        timestr[11] = arr[4] % 10 + '0';
        timestr[12] = ':';
        timestr[13] = (arr[5] > 9) ? arr[0] / 10 + '0' : '0';
        timestr[14] = arr[5] % 10 + '0';
        timestr[15] = '\0';
        

        if (tipe == MKDIR_CODE)
            fprintf(logFile, "INFO::%s::MKDIR::%s\n", timestr,lama);
        else if (tipe == MKNOD_CODE)
            fprintf(logFile, "INFO::%s::MKNOD::%s\n", timestr,lama);
        else if (tipe == RENAME_CODE)
            fprintf(logFile, "INFO::%s::RENAME::%s::%s\n", timestr,lama,baru);
        else if (tipe == RMDIR_CODE)
            fprintf(logFile, "WARNING::%s::RMDIR::%s\n", timestr,lama);
        else if(tipe == UNLINK_CODE)
            fprintf(logFile, "WARNING::%s::UNLINK::%s\n", timestr,lama);

        fclose(logFile);
    }
    ```

    Untuk memudahkan pembuatan if-else statement, didefinisikan beberapa constant berupa code yang digunakan untuk menentukan command yang telah dijalankan. Constant tersebut didefinisikan seperti berikut:

    ```c
    #define MKDIR_CODE 1
    #define MKNOD_CODE 2
    #define RENAME_CODE 3
    #define RMDIR_CODE 4
    #define UNLINK_CODE 5
    ```

    Pemanggilan fungsi:
        
    + Line 418: `IANLog(path, "", MKDIR_CODE);`  
    + Line 468: `IANLog(sumber, tujuan, RENAME_CODE);`
    + Line 599: `IANLog(path, "", RMDIR_CODE);`
    + Line 613: `IANLog(path, "", UNLINK_CODE);`
    + Line 627: `IANLog(path, "", MKNOD_CODE);`

### Dokumentasi

+ `a.` output vigenere_encode()
    ![output vigenere_encode()](/doku/soal2/vigenere_encode.png) 
  
+ `b.` enkripsi setelah command `mv`
    ![enkripsi setelah command `mv`](/doku/soal2/encrypt.png) 
+ `c.` output vigenere_decode()
    ![output vigenere_decode()](/doku/soal2/vigenere_decode.png)
+ `d.` n/a
+ `e.`
  1. kondisi awal
    ![kondisi awal](/doku/soal2/before_mkdir_dir.png) 
  2. setelah `mkdir IAN_test`
   ![setelah `mkdir IAN_test` dir](/doku/soal2/after_mkdir_dir.png) 
   ![setelah `mkdir IAN_test` log](/doku/soal2/after_mkdir_log.png) 
  3. setelah `echo "test" >> testFile.txt`
   ![setelah `echo "test" >> testFile.txt`](/doku/soal2/after_mknod_log.png) 
  4. setelah `mv IAN_test ian_test`
   ![setelah `mv IAN_test ian_test`](/doku/soal2/after_rename_log.png) 
  5. setelah `rm testFile.txt`
   ![setelah `rm testFile.txt`](/doku/soal2/after_unlink_log.png) 
  6. setelah `rmdir ian_test`
   ![setelah `rmdir ian_test`](/doku/soal2/after_rmdir_log.png) 

## Soal 3

Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan ketentuan:

   + `a.` Jika suatu direktori dibuat dengan awalan `nam_do-saq_`, maka direktori tersebut akan menjadi sebuah direktori spesial.
   + `b.` Jika suatu direktori di-rename dengan memberi awalan `nam_do-saq_`,maka direktori tersebut akan menjadi sebuah direktori spesial.
   + `c.` Apabila direktori yang terenkripsi di-rename dengan menghapus `nam_do-saq_` pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.
   + `d.` Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori `Animeku_` maupun `IAN_` namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive `Animeku_` dan `IAN_` tetap berjalan pada subdirektori).
   + `e.` Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi **uppercase insensitive** dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

Contoh: jika pada direktori asli namanya adalah `isHaQ_KEreN.txt` maka pada fuse akan
menjadi `ISHAQ_KEREN.txt.1670`. 1670 berasal dari biner 11010000110

### Penyelesaian

   + `a./b./c.` dekripsi akan dilakukan melalui fungsi `namdosaq_decode()` yang dipanggil pada line:
       +  364 fungsi `xmp_getattr()` 
       +  407 fungsi `xmp_readdir()`
       +  565 fungsi `xmp_open()`
       +  601 fungsi `xmp_read()`
       +  642 fungsi `xmp_write()`<br>

     untuk menerapkan perilaku enkripsi pada folder dengan prefiks **'nam_do-saq_'**  
  
   + `d.` enkripsi akan dilakukan melalui fungsi `namdosaq_encode()` yang dipanggil pada line 444 fungsi `readdir()` yang terlihat seperti berikut:
        ```c
        static int xmp_readdir(const char *path,  void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
            char *strCheck;
            int flag;

            // printf(">>>IN READDIR %s\n", path);
            //if ((strCheck = strstr(path, awalan[0])) != NULL) {
                //flag = 1;
                //if (strCheck != NULL)
                    //wibu_decode(strCheck);
            //}


            // else if buat ngehandle kalo awalannya IAN
            //else if ((strCheck = strstr(path, awalan[1])) != NULL) {
                //flag = 2;
                //if (strCheck != NULL) {
                    // printf("IN DECODE VIGENERE %s\n", strCheck);
                    //vigenere_decode(strCheck);
                //}
            //}
            // else if buat ngehandle kalo awalannya IAN

            else if ((strCheck = strstr(path, awalan[2])) != NULL) {
                flag = 3;
                if (strCheck != NULL) {
                    // printf("IN DECODE NAMDOSAQ %s\n", strCheck);
                    namdosaq_decode(strCheck, path);
                }
            }
            // printf(">>>AFTER DECODE READDIR %s\n", path);


            char fpath[1000];
            if (strcmp(path,"/") == 0) {
                path = dirPath;
                sprintf(fpath, "%s", path);
            }
            else
                sprintf(fpath, "%s%s", dirPath, path);

            int result = 0;
            DIR *dp;
            struct dirent *dir;
            (void)offset;
            (void)fi;

            dp = opendir(fpath);
            if (dp == NULL)
                return -errno;

            while ((dir = readdir(dp)) != NULL) {
                struct stat st;
                memset(&st, 0, sizeof(st));
                st.st_ino = dir->d_ino;
                st.st_mode = dir->d_type << 12;
                if (strCheck != NULL) {
                    //if (flag == 1) wibu_encode(dir->d_name);
                    

                    // else if buat ngehandle kalo awalannya IAN
                    //else if (flag == 2) vigenere_encode(dir->d_name);
                    // else if buat ngehandle kalo awalannya IAN

                    else if (flag == 3){
                        if(dir->d_type == DT_REG){
                            namdosaq_encode(dir->d_name, path);
                        }
                    }

                }
                result = (filler(buf, dir->d_name, &st, 0));
                if (result != 0) break;
            }
            closedir(dp);
            return 0;
        }
        ```
  
   + `e.` pertama dibuatkan fungsi `is_regular_file()` yang digunakan untuk mengetahui apakah sebuah path merupakan file atau tidak. Fungsi tersebut terlihat seperti berikut:
  
        ```c
        int is_regular_file(char* str, const char* path){
            char fpath[1000];
            sprintf(fpath, "%s%s", dirPath, path);
            // printf("\nSTR IN NDS:\t%s\nPATH IN NDS:\t%s\n", str, fpath);
            // for(int i = strlen(fpath) - 1; i >= 0; i--){
            // 	if(fpath[i] == '/') break;
            // 	fpath[i] = '\0';
            // }
            // printf("FORMATTED PATH IN NDS:\t%s\n", fpath);

            DIR* dp = opendir(fpath);
            if (dp){
                struct dirent *dir;
                strcpy(fpath, str);
                char* token = strtok(fpath, "/");
                while(token){
                    char* temp = token;
                    token = strtok(NULL, "/");
                    if(!token){
                        token = temp;
                        break;
                    }
                }

                while ((dir = readdir(dp)) != NULL) {
                    struct stat st;
                    memset(&st, 0, sizeof(st));
                    st.st_ino = dir->d_ino;
                    st.st_mode = dir->d_type << 12;
                    // printf("DIR->D_NAME:\t%s\nSTR:\t%s\nIS_SAME:\t%d\n", dir->d_name, token, !strcmp(token, dir->d_name));
                    if(dir->d_type == DT_REG && !strcmp(str, dir->d_name)) return 1;
                }
            }

            return 0;
        }
        ```

        selanjutnya dibuat fungsi `namdosaq_encode()` yang akan mengenkripsi string menjadi sesuai dengan soal. Fungsi tersebut terlihat seperti berikut:

        ```c
        void namdosaq_encode(char* str, const char* path){
            // printf("&&&& IN NDS ENCODE %s\n", str);
            if (strcmp(str,".") == 0 || strcmp(str, "..") == 0) return;
            // char fpath[1000];
            // sprintf(fpath, "%s%s", dirPath, path);
            // printf("namdosaq_encode: %s\t%s\nDT_REG: %d\n", str, path, is_regular_file(str));
            char fpath[1000];
            sprintf(fpath, "%s%s", dirPath, path);
            if(is_regular_file(str, path)){
                int total = 0;
                int digit = 0;
                char* token = strtok(str, ".");
                int binary[strlen(token)];
                for(int i = 0; i < strlen(token); i++){
                    if(token[i] >= 97 && token[i] <= 122){
                        binary[i] = 1;
                    }else binary[i] = 0;
                    // printf("%d ", binary[i]);
                }

                for(int i = 0; i < strlen(token); i++) total += binary[i] * pow(2, strlen(token) - i - 1);

                int temp = total;
                do{
                    temp /= 10;
                    digit++;
                }while(temp);

                str[strlen(token)] = '.';
                str[strlen(token) + 1] = '\0';
                token = strtok(NULL, ".");
                namdosaq_encode_helper(str, total, digit);
                // printf("&&&& AFTER NDS ENCODE %s\n", str);
            }
        }
        ```

        Untuk mengembalikan nama file menjadi seperti semula, dibuat fungsi `namdosaq_decode()` yang terlihat seperti berikut:

        ```c
        void namdosaq_decode(char* str, const char* path){
            if (strcmp(str,".") == 0 || strcmp(str,"..") == 0 || strstr(str, "/") == NULL)
                return;

            // printf(">>>>>>>>>>>>> IN MDS DECODER %s\n", str);

            //format XXXX.EXT.00000
            int period_count = 0;
            int difference = 0;
            int start;
            int end;
            int period_1_idx = -9999;
            int digit = 0;
            for(int i = strlen(str) - 1; i >= 0; i--){
                if(str[i] == '\0') continue;
                if(str[i] == '.'){
                    period_count++;
                    if(period_1_idx == -9999) period_1_idx = i;
                    end = i;
                }if(!period_count) {
                    if(str[i] >= '0' && str[i] <= '9'){
                        difference += (str[i] - '0') * pow(10, digit++);
                        // printf("FOUND NUMBER %c DIFFERENCE IS %d\n", str[i], difference);
                    }else {
                        return;
                    }
                }if(str[i] == '/') start = i + 1;
            }

            // printf(">>>>>>>>>>>>> OUT NDS DECODER ");
            for(int i = start; i < end; i++){
                printf("%c", str[i]);
            }
            printf("\n");
            // printf(">>>>>>>>>>>>> OUT DIFFERENCE %d\n", difference);
            // printf(">>>>>>>>>>>>> OUT BINARY ");
            digit = end - start;
            short binary[digit];
            for(int i = digit - 1; i >= 0; i--){
                binary[i] = difference % 2;
                difference /= 2;
            }
            for(int i = 0; i < digit; i++) printf("%d ", binary[i]);
            printf("\n");

            int j = 0;
            for(int i = start; i < end; i++){
                if(binary[j++]) str[i] += 32;
            }
            str[period_1_idx] = '\0';
            
            // printf(">>>>>>>>>>>>> COMPLETE NDS DECODER %s\n", str);
        }
        ```
        

### Dokumentasi

+ `a.` 
    1. setelah pembuatan nam_do-saq_
    ![setelah pembuatan nam_do-saq_](/doku/soal3/A2.png) 
    2. isi nam_do-saq_ 
    ![isi nam_do-saq_](/doku/soal3/A3.png) 
  
+ `b.`
    1. sebelum penamaan nam_do-saq_
    ![sebelum penamaan nam_do-saq_](/doku/soal3/B1.png) 
    2. setelah penamaan nam_do-saq_
    ![setelah penamaan nam_do-saq_](/doku/soal3/B2.png) 
    3. isi nam_do-saq_ 
    ![isi nam_do-saq_ ](/doku/soal3/B3.png) 

+ `c.`
    1. sebelum penamaan nam_do-saq
    ![sebelum penamaan nam_do-saq](/doku/soal3/C1.png) 
    2. setelah penamaan nam_do-saq
    ![setelah penamaan nam_do-saq](/doku/soal3/C2.png) 
    3. isi nam_do-saq 
    ![isi nam_do-saq](/doku/soal3/C3.png)

+ `d.`
    1. isi nam_do-saq
    ![isi nam_do-saq](/doku/soal3/D1.png) 
    2. isi nam_do-saq/Animeku_
    ![isi nam_do-saq/Animeku_](/doku/soal3/D2.png) 
    1. isi nam_do-saq_
    ![isi nam_do-saq_](/doku/soal3/D3.png)  
    1. isi nam_do-saq_/Animeku_
    ![isi nam_do-saq_/Animeku_](/doku/soal3/D4.png) 
    
+ `e.`
    1. hasil encode
    ![sebelum encode](/doku/soal3/E1.png) 
