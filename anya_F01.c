#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <malloc.h>
#include <math.h>

#define MKDIR_CODE 1
#define MKNOD_CODE 2
#define RENAME_CODE 3
#define RMDIR_CODE 4
#define UNLINK_CODE 5

static const char *dirPath = "/home/nfpurnama/sfm4/temp";
char awalan[3][16] = {"Animeku_", "IAN_", "nds_"};

void wibuLog(const char *lama, char *baru, int tipe) {
	char *namaLog = "Wibu.log";
	char logPath[100];
	sprintf(logPath, "%s/%s", dirPath, namaLog);
	FILE *logFile = fopen(logPath, "a");

	if (tipe == 0)
		fprintf(logFile, "MKDIR\tterenkripsi\t%s\t->\t%s\n", lama, baru);
	else if (tipe == 1)
		fprintf(logFile, "RENAME\tterenkripsi\t%s\t->\t%s\n", lama, baru);
	else if (tipe == 2)
		fprintf(logFile, "RENAME\tterdecode\t%s\t->\t%s\n", lama, baru);
	fclose(logFile);
}

void wibu_encode (char *str) {
	printf("@@ IN ENCODE WIBU %s\n", str);
	if (strcmp(str,".") == 0 || strcmp(str, "..") == 0) return;
	int length = strlen(str);
	for (int i=0; i < length; i++) {
		if (str[i] == '/') continue;
		if (str[i] == '.') break;

		if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] = 'Z' + 'A' - str[i];
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] = 'a' + (str[i] -'a' + 13) % 26;
	}
	printf("@@ IN ENCODE WIBU %s\n", str);
}

void wibu_decode (char *str) {
	if (strcmp(str,".") == 0 || strcmp(str,"..") == 0 || strstr(str, "/") == NULL)
		return;

	printf("@@ IN DECODE WIBU %s\n", str);
	int length = strlen(str), s = 0;
	for (int i= length; i>=0; i--) {
		if (str[i] == '/') break;
		if (str[i] == '.') {
			length = i;
			break;
		}
	}
	for (int i= 0; i < length; i++) {
		if (str[i] == '/') {
			s = i+1;
			break;
		}
	}
	for (int i =s; i < length; i++) {
		if (str[i] == '/') continue;
		if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] = 'Z' + 'A' - str[i];
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] = 'a' + (str[i] - 'a' + 13) % 26;
	}
	printf("@@ OUT DECODE WIBU %s\n", str);
}

void IANLog(const char *lama, const char *baru, int tipe) {
	char *namaLog = "hayolongapain_F01.log";
	char logPath[100];
	sprintf(logPath, "%s/%s", dirPath, namaLog);
	FILE *logFile = fopen(logPath, "a");

	struct tm* ptr;
	time_t tm = time(NULL);
	ptr = localtime(&tm);
	int arr[6];
	arr[0] = ptr->tm_mday;
	arr[1] = ptr->tm_mon;
	arr[2] = ptr->tm_year;
	arr[3] = ptr->tm_hour;
	arr[4] = ptr->tm_min;
	arr[5] = ptr->tm_sec;

	char timestr[16];
	timestr[0] = (arr[0] > 9) ? arr[0] / 10 + '0' : '0'; 
	timestr[1] = arr[0] % 10 + '0';
	timestr[2] = (arr[1] > 9) ? arr[0] / 10 + '0' : '0';
	timestr[3] = arr[1] % 10 + '0';
	timestr[4] = (arr[2] > 9) ? arr[0] / 10 + '0' : '0';
	timestr[5] = arr[2] % 10 + '0';
	timestr[6] = '-';
	timestr[7] = (arr[3] > 9) ? arr[0] / 10 + '0' : '0';
	timestr[8] = arr[3] % 10 + '0';
	timestr[9] = ':';
	timestr[10] = (arr[4] > 9) ? arr[0] / 10 + '0' : '0';
	timestr[11] = arr[4] % 10 + '0';
	timestr[12] = ':';
	timestr[13] = (arr[5] > 9) ? arr[0] / 10 + '0' : '0';
	timestr[14] = arr[5] % 10 + '0';
	timestr[15] = '\0';
	

	if (tipe == MKDIR_CODE)
		fprintf(logFile, "INFO::%s::MKDIR::%s\n", timestr,lama);
	else if (tipe == MKNOD_CODE)
		fprintf(logFile, "INFO::%s::MKNOD::%s\n", timestr,lama);
	else if (tipe == RENAME_CODE)
		fprintf(logFile, "INFO::%s::RENAME::%s::%s\n", timestr,lama,baru);
	else if (tipe == RMDIR_CODE)
		fprintf(logFile, "WARNING::%s::RMDIR::%s\n", timestr,lama);
	else if(tipe == UNLINK_CODE)
		fprintf(logFile, "WARNING::%s::UNLINK::%s\n", timestr,lama);

	fclose(logFile);
}

void vigenere_encode (char *str) {
	printf("## OUT DECODE VIGENERE %s\n", str);
    if (strcmp(str, ".") == 0 || strcmp(str,"..") == 0) return;
	int length = strlen(str);
	char key[] = "INNUGANTENG";
    int key_len = strlen(key);

    int i;
    for(i = 0; i < length; i++) {
        if (str[i] == '/') continue;
        if((str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)){
            int is_capital = 0;
            if(str[i] >= 65 && str[i] <= 90) is_capital = 1;
            if(is_capital){
                str[i] = ((str[i] + key[i % (key_len)] - 130) % 26 + 65);
            }else{
                str[i] = ((str[i] + key[i % (key_len)] + 32 - 194) % 26 + 97);
            }
        }
	}
	printf("## OUT DECODE VIGENERE %s\n", str);
}

void vigenere_decode (char *str) {
    if (strcmp(str,".") == 0 || strcmp(str,"..") == 0 || strstr(str, "/") == NULL)
		return;

	printf("## IN DECODE VIGENERE %s\n", str);
	
	char key[] = "INNUGANTENG";
    int key_len = strlen(key);

    int length = strlen(str);
    int s = 0;
	for (int i = length; i >= 0; i--) {
		if (str[i] == '/') break;
		if (str[i] == '.') {
			length = i;
			break;
		}
	}for (int i= 0; i < length; i++) {
		if (str[i] == '/') {
			s = i + 1;
			break;
		}
	}

	printf("## CURR DECODE VIGENERE %s\n", str + s);

	for(int i = s; i < length; i++) {
        if((str[i] >= 65 && str[i] <= 90) || (str[i] >= 97 && str[i] <= 122)){
            if(str[i] >= 65 && str[i] <= 90){
                str[i] = (str[i] - (key[(i - s) % (key_len)]) < 0) ? str[i] - (key[(i - s) % (key_len)]) + 26 + 65 : str[i] - (key[(i - s) % (key_len)]) + 65;
            }else{
                str[i] = (str[i] - (key[(i - s) % (key_len)] + 32) < 0) ? str[i] - (key[(i - s) % (key_len)] + 32) + 26 + 97 : str[i] - (key[(i - s)  % (key_len)] + 32) + 97;
            }
        }
	}

	printf("## OUT DECODE VIGENERE %s\n", str);
}

void namdosaq_encode_helper(char* str, int total, int digit){
    char* temp = (char*) malloc(sizeof(char) * (strlen(str) + digit + 2));
    int i;
    for(i = 0; i < strlen(str); i++){
        temp[i] = str[i];
        if(temp[i] == '.') break;
        if(temp[i] >= 'a' && temp[i] <= 'z') temp[i] -= 32;
    }
    for(i=i; i < strlen(str); i++) temp[i] = str[i];

    temp[i++] = '.';
    for(int j = digit - 1; j >= 0; j--){
        int res = total / (int) pow(10, j);
        total %= (int) pow(10, j);
        temp[i++] = '0' + res;
    }
    temp[i++] = '\0';
    strcpy(str, temp);
}

int is_regular_file(char* str, const char* path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirPath, path);
	printf("\nSTR IN NDS:\t%s\nPATH IN NDS:\t%s\n", str, fpath);
	// for(int i = strlen(fpath) - 1; i >= 0; i--){
	// 	if(fpath[i] == '/') break;
	// 	fpath[i] = '\0';
	// }
	printf("FORMATTED PATH IN NDS:\t%s\n", fpath);

	DIR* dp = opendir(fpath);
	if (dp){
		struct dirent *dir;
		strcpy(fpath, str);
		char* token = strtok(fpath, "/");
		while(token){
			char* temp = token;
			token = strtok(NULL, "/");
			if(!token){
				token = temp;
				break;
			}
		}

		while ((dir = readdir(dp)) != NULL) {
			struct stat st;
			memset(&st, 0, sizeof(st));
			st.st_ino = dir->d_ino;
			st.st_mode = dir->d_type << 12;
			printf("DIR->D_NAME:\t%s\nSTR:\t%s\nIS_SAME:\t%d\n", dir->d_name, token, !strcmp(token, dir->d_name));
			if(dir->d_type == DT_REG && !strcmp(str, dir->d_name)) return 1;
		}
	}

	return 0;
}

void namdosaq_encode(char* str, const char* path){
	// printf("&&&& IN NDS ENCODE %s\n", str);
	if (strcmp(str,".") == 0 || strcmp(str, "..") == 0) return;
	// char fpath[1000];
	// sprintf(fpath, "%s%s", dirPath, path);
	// printf("namdosaq_encode: %s\t%s\nDT_REG: %d\n", str, path, is_regular_file(str));
	char fpath[1000];
	sprintf(fpath, "%s%s", dirPath, path);
	if(is_regular_file(str, path)){
		int total = 0;
		int digit = 0;
		char* token = strtok(str, ".");
		int binary[strlen(token)];
		for(int i = 0; i < strlen(token); i++){
			if(token[i] >= 97 && token[i] <= 122){
				binary[i] = 1;
			}else binary[i] = 0;
			// printf("%d ", binary[i]);
		}

		for(int i = 0; i < strlen(token); i++) total += binary[i] * pow(2, strlen(token) - i - 1);

		int temp = total;
		do{
			temp /= 10;
			digit++;
		}while(temp);

		str[strlen(token)] = '.';
		str[strlen(token) + 1] = '\0';
		token = strtok(NULL, ".");
		namdosaq_encode_helper(str, total, digit);
		// printf("&&&& AFTER NDS ENCODE %s\n", str);
	}
}

void namdosaq_decode(char* str, const char* path){
	if (strcmp(str,".") == 0 || strcmp(str,"..") == 0 || strstr(str, "/") == NULL)
		return;

	printf(">>>>>>>>>>>>> IN MDS DECODER %s\n", str);

	//format XXXX.EXT.00000
	int period_count = 0;
	int difference = 0;
	int start;
	int end;
	int period_1_idx = -9999;
	int digit = 0;
	for(int i = strlen(str) - 1; i >= 0; i--){
		if(str[i] == '\0') continue;
		if(str[i] == '.'){
			period_count++;
			if(period_1_idx == -9999) period_1_idx = i;
			end = i;
		}if(!period_count) {
			if(str[i] >= '0' && str[i] <= '9'){
				difference += (str[i] - '0') * pow(10, digit++);
				printf("FOUND NUMBER %c DIFFERENCE IS %d\n", str[i], difference);
			}else {
				return;
			}
		}if(str[i] == '/') start = i + 1;
	}

	printf(">>>>>>>>>>>>> OUT NDS DECODER ");
	for(int i = start; i < end; i++){
		printf("%c", str[i]);
	}
	printf("\n");
	printf(">>>>>>>>>>>>> OUT DIFFERENCE %d\n", difference);
	printf(">>>>>>>>>>>>> OUT BINARY ");
	digit = end - start;
	short binary[digit];
	for(int i = digit - 1; i >= 0; i--){
		binary[i] = difference % 2;
		difference /= 2;
	}
	for(int i = 0; i < digit; i++) printf("%d ", binary[i]);
	printf("\n");

	int j = 0;
	for(int i = start; i < end; i++){
		if(binary[j++]) str[i] += 32;
	}
	str[period_1_idx] = '\0';
	
	printf(">>>>>>>>>>>>> COMPLETE NDS DECODER %s\n", str);
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
	// printf("getattr\t%s\n", path);
	char *strCheck;
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		if (strCheck != NULL) wibu_decode(strCheck);
	}

	// else if buat ngehandle kalo awalannya IAN
	else if ((strCheck = strstr(path, awalan[1])) != NULL) {
		if (strCheck != NULL) {
			// printf("IN DECODE VIGENERE %s\n", strCheck);
			vigenere_decode(strCheck);
		}
	}
	// else if buat ngehandle kalo awalannya IAN

	else if ((strCheck = strstr(path, awalan[2])) != NULL) {
		if (strCheck != NULL) { 
			namdosaq_decode(strCheck, path);
		}
	}


	int result;
	char fpath[1000];

	sprintf(fpath, "%s%s", dirPath, path);
	// printf("getattr: %s\n", fpath);
	result = lstat(fpath, stbuf);

	if (result == -1) return -errno;

	return 0;
}

static int xmp_readdir(const char *path,  void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
	char *strCheck;
	int flag;

	// printf(">>>IN READDIR %s\n", path);
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		flag = 1;
		if (strCheck != NULL)
			wibu_decode(strCheck);
	}


	// else if buat ngehandle kalo awalannya IAN
	else if ((strCheck = strstr(path, awalan[1])) != NULL) {
		flag = 2;
		if (strCheck != NULL) {
			// printf("IN DECODE VIGENERE %s\n", strCheck);
			vigenere_decode(strCheck);
		}
	}
	// else if buat ngehandle kalo awalannya IAN

	else if ((strCheck = strstr(path, awalan[2])) != NULL) {
		flag = 3;
		if (strCheck != NULL) {
			// printf("IN DECODE NAMDOSAQ %s\n", strCheck);
			namdosaq_decode(strCheck, path);
		}
	}
	// printf(">>>AFTER DECODE READDIR %s\n", path);


	char fpath[1000];
	if (strcmp(path,"/") == 0) {
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
		sprintf(fpath, "%s%s", dirPath, path);

	int result = 0;
	DIR *dp;
	struct dirent *dir;
	(void)offset;
	(void)fi;

	dp = opendir(fpath);
	if (dp == NULL)
		return -errno;

	while ((dir = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if (strCheck != NULL) {
			if (flag == 1) wibu_encode(dir->d_name);
			

			// else if buat ngehandle kalo awalannya IAN
			else if (flag == 2) vigenere_encode(dir->d_name);
			// else if buat ngehandle kalo awalannya IAN

			else if (flag == 3){
				if(dir->d_type == DT_REG){
					namdosaq_encode(dir->d_name, path);
				}
			}

		}
		result = (filler(buf, dir->d_name, &st, 0));
		if (result != 0) break;
	}
	closedir(dp);
	return 0;
}

static int xmp_mkdir(const char *path, mode_t mode) {
	printf("\n\nMKDIR\n\n");

	if(strstr(path, awalan[0]) != NULL) {
		char fpath[1000];
		if (strcmp(path, "/") == 0) {
			path = dirPath;
			sprintf(fpath, "%s", path);
		}
		else sprintf(fpath, "%s%s", dirPath, path);
		int result = mkdir(fpath, mode);
		char *folderPath = strstr(path, awalan[0]);
		if (folderPath != NULL)
			wibuLog(fpath, fpath, 0);
		if (result == -1) return -errno;
	}


	// else if buat ngehandle kalo awalannya IAN
	else if(strstr(path, awalan[1]) != NULL) {
		// printf("%s", strstr(path, awalan[1]));
		char fpath[1000];
		if (strcmp(path, "/") == 0) {
			path = dirPath;
			sprintf(fpath, "%s", path);
		}
		else sprintf(fpath, "%s%s", dirPath, path);
		int result = mkdir(fpath, mode);
		char *folderPath = strstr(path, awalan[1]);
		if (folderPath != NULL)
			IANLog(path, "", MKDIR_CODE);
		if (result == -1) return -errno;
	}
	// else if buat ngehandle kalo awalannya IAN
	
	else{
		char fpath[1000];
		if (strcmp(path, "/") == 0) {
			path = dirPath;
			sprintf(fpath, "%s", path);
		}
		else sprintf(fpath, "%s%s", dirPath, path);
		int result = mkdir(fpath, mode);
		if (result == -1) return -errno;	
	}

	return 0;
}

static int xmp_rename(const char *sumber, const char *tujuan) {
	int flag;
	if (strstr(sumber, awalan[0]) != NULL) flag = 1;

	// else if buat ngehandle kalo awalannya IAN
	else if (strstr(sumber, awalan[1]) != NULL) flag = 2;
	// else if buat ngehandle kalo awalannya IAN

	else if (strstr(sumber, awalan[2]) != NULL) flag = 3;




	char fileSumber[1000], fileTujuan[1000];
	sprintf(fileSumber, "%s%s", dirPath, sumber);
	sprintf(fileTujuan, "%s%s", dirPath, tujuan);
	int result;
	if (flag == 1) {
		if (strstr(fileTujuan, awalan[0]) != NULL)
			wibuLog(fileSumber, fileTujuan, 1);
		else if (strstr(fileSumber, awalan[0]) != NULL) 
			wibuLog(fileSumber, fileTujuan, 2);
		result = rename (fileSumber, fileTujuan);
	}


	// else if buat ngehandle kalo awalannya IAN
	else if (flag == 2) {
		result = rename (fileSumber, fileTujuan);
		if(result != -1) IANLog(sumber, tujuan, RENAME_CODE);
	}
	// else if buat ngehandle kalo awalannya IAN

	else {
		result = rename(fileSumber, fileTujuan);
	}

	if (result == -1)
		return -errno;
	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
	char *strCheck;

	// printf(">>>IN OPEN %s\n", path);
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		if (strCheck != NULL) wibu_decode(strCheck);
	}


	// else if buat ngehandle kalo awalannya IAN
	else if ((strCheck = strstr(path, awalan[1])) != NULL) {
		if (strCheck != NULL) vigenere_decode(strCheck);
	}
	// else if buat ngehandle kalo awalannya IAN

	else if ((strCheck = strstr(path, awalan[2])) != NULL) {
		if (strCheck != NULL) namdosaq_decode(strCheck, path);
	}
	// printf(">>>AFTER DECODE OPEN %s\n", path);

	char fpath[1000];
	if (strcmp(path, "/") == 0) {
		path =dirPath;
		sprintf(fpath, "%s", path);
	}
	else
		sprintf(fpath, "%s%s", dirPath, path);

	int result;
	result = open(fpath, fi->flags);
	if (result == -1) return -errno;
	close(result);
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	
	// printf(">>>IN READ %s\n", path);
	
	char *strCheck;
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		if (strCheck != NULL) wibu_decode(strCheck);
	}


	// else if buat ngehandle kalo awalannya IAN
	else if ((strCheck = strstr(path, awalan[1])) != NULL) {
		if (strCheck != NULL) vigenere_decode(strCheck);
	}
	// else if buat ngehandle kalo awalannya IAN

	else if ((strCheck = strstr(path, awalan[2])) != NULL) {
		if (strCheck != NULL) namdosaq_decode(strCheck, path);
	}

	// printf(">>>AFTER DECODE READ %s\n", path);

	char fpath[1000];
	if (strcmp(path, "/") == 0) {
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
		sprintf(fpath, "%s%s", dirPath, path);

	int result = 0;
	int fd = 0;
	(void)fi;

	fd = open(fpath, O_RDONLY);
	if (fd == -1) return -errno;

	result = pread(fd, buf, size, offset);
	if (result == -1) result = -errno;

	close(fd);
	return result;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	char *strCheck;
	if ((strCheck = strstr(path, awalan[0])) != NULL) {
		if (strCheck != NULL) wibu_decode(strCheck);
	}


	// else if buat ngehandle kalo awalannya IAN
	else if ((strCheck = strstr(path, awalan[1])) != NULL) {
		if (strCheck != NULL) vigenere_decode(strCheck);
	}
	// else if buat ngehandle kalo awalannya IAN

	else if ((strCheck = strstr(path, awalan[2])) != NULL) {
		if (strCheck != NULL) namdosaq_decode(strCheck, path);
	}

	char fpath[1000];
	if (strcmp(path, "/")== 0) {
		path = dirPath;
		sprintf(fpath, "%s", path);
	}
	else
		sprintf(fpath, "%s%s", dirPath, path);

	int result;
	int fd;
	(void)fi;

	fd = open(fpath, O_WRONLY);
	if (fd == -1) return -errno;

	result = pwrite(fd, buf, size, offset);
	if (result == -1) result = -errno;
	close(fd);
	return result;
}

static int xmp_rmdir(const char* path){
	int result = 0;
	char fpath[1000];

	sprintf(fpath, "%s%s", dirPath, path);
	printf("RMDIR %s\n", fpath);

	result = rmdir(fpath);
	if(result == -1) return -errno;
	IANLog(path, "", RMDIR_CODE);

	return result;
}

static int xmp_unlink(const char* path){
	int result = 0;
	char fpath[1000];

	sprintf(fpath, "%s%s", dirPath, path);
	printf("RM %s\n", fpath);

	result = remove(fpath);
	if(result == -1) return -errno;
	IANLog(path, "", UNLINK_CODE);

	return result;
}

static int xmp_mknod(const char* path, mode_t mode, dev_t dev){
	int result = 0;
	char fpath[1000];

	sprintf(fpath, "%s%s", dirPath, path);
	printf("MKNOD %s\n", fpath);

	result = mknod(fpath, mode, dev);
	if(result == -1) return -errno;
	IANLog(path, "", MKNOD_CODE);

	return result;
}

static struct fuse_operations xmp_oper = {
	.getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.mkdir = xmp_mkdir,
	.mknod = xmp_mknod,
	.rename = xmp_rename,
	.rmdir = xmp_rmdir,
	.unlink = xmp_unlink,
	.read = xmp_read,
	.open = xmp_open,
	.write = xmp_write,
};

int main (int argc, char *argv[]) {
	umask(0);
	return fuse_main(argc, argv, &xmp_oper, NULL);
}
